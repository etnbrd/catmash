I designed this small project as an assignement for an interview.
I spent roughly two days on this.

## Assignement

The goal of the assignement was the following.

Develop a small web app that allows to find the cutest cat, based on the Facemash UX and some given data.
The app should be composed of two pages:
- a page to vote
- a page to see all the cats with their score

The application must be usable with Google Chrome.
The choice of technology is left free.
The source of the project shall be made available publicly.
The web app itself shall be made available publicly.

After this assignement, the code review will insist on the code quality (structrue, organisation, variables and methods names, etc...), the commits frequency and messages, as well as the global experience provided by the application.
It is not necessary for the project to be entierely done.

---

I took this assignement as an opportunity to test new technologies I wanted to give a look to for some time: [Next.js](https://github.com/zeit/next.js/) and [Apollo/GraphQL](https://www.apollographql.com/).

While I was implementing this, I wrote my progression in the timestamped `journal` file, beside.
Unfortunately, I used a custom script (`npm run journal`) to do so, and it failed me several times.

## Limitations

Because I had a limited time, this application has some limitations that I am aware of.

### Implementation

Being my first time with both Next.js and Apollo, I was discovering the technologies during the implementation.
There are numerous resources that helped me, however being constrained by time, I couldn't solve all the weakness and inconsistencies of my implementation.
The most critical one was my inability to use local GraphQL request.
I planned to rely on Apollo to hold both the server side and the client side states.
Apollo client should make it really easy to do so, however, I failed to use it correctly.
I believe it has to do with Next.js and its use of SSR, but couldn't pinpoint the exact problem.

The consequence being that in `front/scenes/mash/state.js`, I must use `client.writeData` to mutate an existing query, instead of issuing a local query.

Also, I didn't took the time to write tests, documentation nor types definitions.

### Bugs

The most problematic bug I noticed is that the boards, if accessed previously, are not taking into account the new mashes until a remash, which causes a complete reset of the cache.
It's linked with the implementation limitation aforementionned.

The second bug I noticed is something I failed to anticipate: a cat should not be able to mash with itself.
In the current implementation, it can, and if that happens, it leads to quite strange bugs.

Also, the remash button should lose focus when clicking.

### Improvements

I could implement a way to send to mashes the cats that have the least mashes.

I could implement a better data persistence layer than the server runtime memory.