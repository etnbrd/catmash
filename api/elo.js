const K = 24; // K factor used in chess

function expectation(player, oponent) {
  return (
    1 / (
      1 + Math.pow(
        10,
        ( oponent.score - player.score )
        / 400
      )
    )
  )
}

function update(player, oponent, result) {
  return player.score + K * ( result - expectation(player, oponent))
}

module.exports = {
  update
}