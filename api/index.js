const { ApolloServer, gql } = require('apollo-server-express');

const elo = require('./elo')

const { images: catsInit } = require('./cats');
const cats = catsInit.map(({ url, id }) => ({ url, id, score: 1000 + Math.random() }))

// GraphQL schema
const typeDefs = gql`
  type Query {
    cats(orderBy: String, limit: Int): [ Cat ],
    mash: Mash
  }

  type Mutation {
    score(winnerId: ID!, looserId: ID!): Cat
  }

  type Mash {
    oponents: [ Cat ]
    winner: Cat
  }

  type Cat {
    id: ID,
    url: String,
    score: Float
  }
`;

// Resolvers
const resolvers = {
  Query: {
    cats: (_, { orderBy, limit = Infinity }) => {
      return (
        orderBy
        ? cats.sort((a, b) => b[orderBy] - a[orderBy])
        : cats
      )
        .slice(0, limit)
        .map(cat => ({
          ...cat,
          score: Math.round(cat.score)
        }))
    },

    mash: () => {
      const index1 = Math.ceil(Math.random() * cats.length);
      const index2 = Math.ceil(Math.random() * cats.length);

      return {
        oponents: [ cats[index1], cats[index2] ],
        winner: null
      }
    }
  },
  Mutation: {
    score: (_, { winnerId, looserId }) => {
      const winner = cats.find(({ id }) => id === winnerId)
      const looser = cats.find(({ id }) => id === looserId)

      winner.score = elo.update(winner, looser, 1)
      looser.score = elo.update(looser, winner, 0)

      return winner
    }
  }
};

module.exports = async function initApi(app) {
  const api = new ApolloServer({ typeDefs, resolvers })
  api.applyMiddleware({ app });
  return api;
}