const express = require('express')

const initApi = require('./api')
const initFront = require('./front')


// Initialize the app
const dev = process.env.NODE_ENV !== 'production'
const port = process.env.PORT || 3000
const url = dev
  ? `http://localhost:${port}`
  : process.env.PUBLIC_URL

console.log(dev ? 'dev - ' : 'prod - ', url)

const app = express()
const api = initApi(app)
const front = initFront(app, dev, url)

Promise.all([ api, front ]).then(([ api, front ]) => {

  app.get('*', (req, res) => {
    return front(req, res)
  })

  app.listen({ port }, (err) => {
    if (err) throw err
    console.log(
      `🚀 Server ready at http://localhost:${port}${api.graphqlPath}`
    )
  })

})
.catch((ex) => {
  console.error(ex.stack)
  process.exit(1)
})