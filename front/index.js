const next = require('next')

module.exports = async function initFront(app, dev, url) {
  const front = next({
    dev,
    dir: __dirname,
    conf: {
      publicRuntimeConfig: {
        url
      }
    }
  })

  await front.prepare()
  return front.getRequestHandler()
}
    