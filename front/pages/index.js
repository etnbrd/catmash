import React from 'react'
import Fame from '../scenes/fame'

import routes from '../routes'

import Container from '../components/container'
import Header from '../components/header'
import Navigation from '../components/navigation'
import Footer from '../components/footer'

const { mash, board } = routes

export default () => (
  <Container>
    <Header />
    <Fame />
    <Navigation links={[ mash, board ]}/>
    <Footer />
  </Container>
)