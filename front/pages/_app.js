import React from 'react'
import App, { Container } from 'next/app'
import Head from 'next/head'
import { ApolloProvider } from 'react-apollo'
import { ThemeProvider } from 'styled-components'

import withApolloClient from '../lib/with-apollo-client'
import theme from '../theme'

class MyApp extends App {
  render () {
    const { Component, pageProps, apolloClient } = this.props
    return (
      <Container>

        <Head>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1" />
          <meta
            charSet="utf-8" />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Montserrat:400,900" />
        </Head>

        <style jsx global>
          {`
            * {
              margin: 0;
              padding: 0;
              font-family: Montserrat;
              font-weight: 900;
            }
            a {
              color: inherit;
              text-decoration: none;
            }
          `}
        </style>

        <ApolloProvider client={ apolloClient }>
          <ThemeProvider theme={theme}>
            <Component { ...pageProps } />
          </ThemeProvider>
        </ApolloProvider>
      </Container>
    )
  }
}

export default withApolloClient(MyApp)