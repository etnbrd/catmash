import React from 'react'

import Mash from '../scenes/mash';

import Container from '../components/container'
import Header from '../components/header'
import Footer from '../components/footer'

export default () => (
  <Container>
    <Header />
    <Mash />
    <Footer />
  </Container>
)