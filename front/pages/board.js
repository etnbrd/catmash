import React from 'react'
import Board from '../scenes/board'

import routes from '../routes'

import Container from '../components/container'
import Header from '../components/header'
import Navigation from '../components/navigation'
import Footer from '../components/footer'

const { mash } = routes

export default () => (
  <Container>
    <Header />
    <Navigation links={[ mash ]}/>
    <Board />
    <Footer />
  </Container>
)