export default {
  index: {
    to: '/',
    label: 'cat⚡mash'
  },
  mash: {
    to: '/mash',
    label: '🗳️ Vote'
  },
  remash: {
    label: '🗳️ Remash'
  },
  board: {
    to: '/board',
    label: '🥇 Board'
  }
}