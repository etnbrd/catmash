import React from 'react'
import styled from 'styled-components'
import Link from 'next/link'

import routes from '../routes'

const Header = styled.div`
  padding: 5rem 0 3rem;
`

const Title = styled.h1`
  text-transform: uppercase;
  text-align: center;

  font-size: 4rem;
  position: relative;

  .bolt {
    color: ${ p => p.theme.colors.text.high };
    font-size: 2em;
    margin: -.3em -.35em;
    position: absolute;
    top: 0;
    z-index: -1;
  }
`

export default () => (
  <Header>
    <Link href={ routes.index.to }>
      <a>
        <Title>cat<span className="bolt">⚡</span>mash</Title>
      </a>
    </Link>
  </Header>
)