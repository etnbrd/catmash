import styled from 'styled-components'

const Footer = styled.div`
  margin: 5rem 0 10rem;
  text-align: center;

  a {
    text-decoration: none;
    font-size: .7rem;
    font-weight: 400;
    color: ${ p => p.theme.colors.text.dim }
  }
`

export default () => (
  <Footer>
    <a href="https://etnbrd.com">etnbrd.com</a>
  </Footer>
)