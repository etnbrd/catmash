import React from 'react'
import styled from 'styled-components'

const ListMarkup = ({ items, render, className }) => (
  <ul className={ className }>
    {
      items.map((item, index) => (
        <li key={item.id}>
          { render(item, index) }
        </li>
      ))
    }
  </ul>
)


const List = styled(ListMarkup)`
  display: flex;
  flex-wrap: wrap;

  li {
    list-style-type: none;
    width: 200pt;
    height: 200pt;
    flex-grow: 1;
  }
`

export default List

export const FameList = styled(List)`
  li:first-child {
    width: 400pt;
    height: 400pt;
  }
`