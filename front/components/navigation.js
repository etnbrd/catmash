import React from 'react'
import styled from 'styled-components'
import Link from 'next/link'

const Anchor = styled.a`
  padding: 1rem 3rem;
  text-transform: uppercase;
  text-align: center;

  color: ${ p => p.theme.colors.text.inverted };

  background: ${ p => p.theme.colors.background.inverted };
  &:hover {
    background: ${ p => p.theme.colors.background.high };
  }

  cursor: pointer;

  width: 100%;
`

const Navigation = styled.div`
  display: flex;
  width: 100%;
  margin: 3rem 0;

  & > ${Anchor}:not(:first-child) {
    margin-left: .2rem;
  }
`

export default ({ links }) => (
  <Navigation>
    {
      links.map(({ label, to }) => (
        typeof to === 'function'
          ? (
              <Anchor key={label} onClick={ to }>{ label }</Anchor>
            )
          : (
              <Link key={label} href={ to }>
                <Anchor>{ label }</Anchor>
              </Link>
            )
      ))
    }
  </Navigation>
)