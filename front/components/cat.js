import React from 'react'
import styled from 'styled-components'

const CatMarkup = ({ url, score, index, className }) => (
  <div className={ className }>
    <div className="index">{ index }</div>
    <div className="score">{ score } cuteness factor</div>
  </div>
)

const Cat = styled(CatMarkup)`
  width: 100%;
  height: 100%;
  background: url(${ p => p.url });
  background-size: cover;
  backgorund-position: center;

  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;

  .index, .score {
    transition: ${ p => p.theme.times.transition.regular };
  }

  .index {
    width: 30pt;
    height: 30pt;
    line-height: 30pt;
    text-align: center;

    border-radius: 50%;

    position: absolute;
    top: 10px;
    right: 10px;
    background: ${ p => p.theme.colors.background.high };
  }

  .score {
    color: ${ p => p.theme.colors.text.inverted };
    background: ${ p => p.theme.colors.background.inverted };
    padding: 1rem;
  }

  &:hover .index,
  &:hover .score {
    opacity: .3;
  }
`

export default Cat