import React from 'react'
import styled from 'styled-components'

import { FameList } from '../../components/list'
import Cat from '../../components/cat'

export default ({ cats }) => (
  <section>
    <FameList
      items= { cats }
      render={
        (cat, index) => <Cat { ...cat } index={ index + 1 } />
      } />
  </section>
)