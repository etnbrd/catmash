import { Query } from 'react-apollo'
import gql from 'graphql-tag'

export const catsQuery = gql`
  query cats {
    cats(orderBy: "score", limit: 3) {
      id
      url
      score
    }
  }
`

export default ({ render }) => (
  <Query query={catsQuery}>
    {({ loading, error, data }) => {

      if (error) return <div>Error loading.</div>
      if (loading) return <div>Loading</div>

      return render({
        cats: data.cats
      })
    }}
  </Query>
)