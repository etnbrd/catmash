import { ApolloConsumer } from 'react-apollo'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'


const catsQuery = gql`
  query Mash {
    mash {
      oponents {
        id
        url
        score
      }
      winner {
        id
        url
        score
      }
    }
  }
`

function vote(client, oponents, winnerIndex) {
  const [ winner, looser ] = winnerIndex === 0
    ? oponents
    : Array.from(oponents).reverse()

  client.writeData({ data: {
    mash: {
      oponents,
      winner: {
        ...winner,
        __typename: 'Cat'
      },
      __typename: 'Mash'
    }
  }})

  client.mutate({
    mutation: gql`
      mutation score($winnerId: ID!, $looserId: ID!) {
        score(winnerId: $winnerId, looserId: $looserId) {
          id
          url
          score
        }
      }
    `,
    variables: {
      winnerId: winner.id,
      looserId: looser.id,
    }
  })
}


function newMash(client) {
  // Hum ... this is really not ideal, but it works for now.
  client.resetStore()
  // client.writeData({
  //   data: {
  //     mash: undefined,
  //   }
  // })
}

export default ({ render }) => (
  <Query query={catsQuery}>
    {({ loading, error, data }) => {

      if (error) return <div>Error loading.</div>
      if (loading) return <div>Loading</div>

      return (
        <ApolloConsumer>
          {
            (client) => render({
              mash: data.mash,
              vote: vote.bind(null, client, data.mash.oponents),
              newMash: newMash.bind(null, client)
            })
          }
        </ApolloConsumer>
      )
    }}
  </Query>
)