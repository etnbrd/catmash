import React from 'react'
import Link from 'next/link'
import styled from 'styled-components'

import Navigation from '../../components/navigation'
import routes from '../../routes'

const CatMarkup = ({ url, id, isWinner, onClick, className }) => (
  <div
    className={ className }
    onClick={ onClick } />
)

const Cat = styled(CatMarkup)`
  background: url(${ p => p.url });
  background-size: cover;

  flex-grow: ${
    // flex-grow is animated only if it is > 0
    p => p.isWinner === undefined
      ? '1'
      : Math.abs(-.0001 + p.isWinner) // false -> 0, true -> 0.0009
  };
  transition: ${ p => p.theme.times.transition.regular };

  &:hover {
    flex-grow: 2;
  }
`

const MashMarkup = ({ oponents, winner, vote, className }) => (
  <section className={ className }>
    {
      oponents.map((cat, index) => (
        <Cat
          { ...cat }
          key={ cat.id }
          onClick={ () => vote(index) }
          isWinner={ winner ? cat.id === winner.id : undefined } />
      ))
    }
  </section>
)


// const heartSize = 7;

//   & > ${Cat}:first-child: {
//     position: relative;

//     &:after {
//       display: block;
//       width: ${ heartSize }rem;
//       height: ${ heartSize }rem;
//       content: '❤';
//       text-align: center;
//       font-size: ${ heartSize }rem;
//       color: ${ p => p.theme.colors.text.high };

//       position: absolute;
//       margin: -${heartSize / 2}rem -${heartSize / 2}rem 0 0;
//       right: 0;
//       top: 50%;
//       pointer-events: none;
//     }
//   }

const Mash = styled(MashMarkup)`
  display: flex;
  height: 250pt;
  max-height: 100vh;
`

export default ({ mash, vote, newMash }) => (
  <>
    <Mash  { ...mash } vote={ vote } />
    <Navigation
      links={ [{
        ...routes.remash,
        to: newMash
      }, {
        ...routes.board
      }] }/>
  </>
)