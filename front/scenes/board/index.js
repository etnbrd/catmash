import React from 'react'

import State from './state'
import Markup from './markup'

export default () => (
  <State render={
    (state) =>
      <Markup { ...state} />
  } />
)