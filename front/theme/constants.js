const colors = {
  white: 'rgb(240, 240, 240)',
  black: 'rgb(15, 15, 15)',
  high: 'rgb(255, 40, 60)',
  shade: 'rgb(170, 170, 170)',
}


export default {
  colors: {
    ...colors,

    text: {
      regular: colors.black,
      inverted: colors.white,
      high: colors.high,
      dim: colors.shade,
    },

    background: {
      regular: colors.white,
      inverted: colors.black,
      high: colors.high,
    }
  },

  times: {
    transition: {
      regular: '.3s',
    }
  }
}